package xeno.mobileapp.ikeastocknotifier;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;

    private boolean mVisible;
    private View rootLayout, logoWrapper, countrySelectorWrapper;
    private Button continueBtn;
    private TextView nameTV;
    private Spinner countrySpinner;
    private String countryValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        logoWrapper = findViewById(R.id.logo_wrapper);
        countrySelectorWrapper = findViewById(R.id.select_country_wrapper);

        mVisible = true;
        hide();

        setupCountrySpinner();
        setupLogo();
        setupContinueBtn();

        String savedCountry = SharedPref.getString(this, R.string.SELECTED_COUNTRY, "N/A");
        switch (savedCountry) {
            case "changeCountry":
                showSelectCountry();
                break;
            case "N/A":
                animateLogoShowSelector();
                break;
            default:
                delaySplash();
                break;
        }
    }

    private void delaySplash() {
        mHideHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, CheckStockActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);
    }

    private void showSelectCountry() {
        logoWrapper.setY(logoWrapper.getY()-250);
        countrySelectorWrapper.setVisibility(View.VISIBLE);
        countrySelectorWrapper.setAlpha(1f);
    }

    private void setupContinueBtn() {
        continueBtn = (Button) findViewById(R.id.continue_btn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashActivity.this, CheckStockActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void setupLogo() {
        rootLayout = findViewById(R.id.root_layout);
        nameTV = (TextView) findViewById(R.id.ikea_stock_textview);

        nameTV.setTypeface(Typeface.createFromAsset(
                getAssets(), "fonts/futurapress.ttf"),
                Typeface.BOLD
        );
    }

    private void setupCountrySpinner() {
        countrySpinner = (Spinner) findViewById(R.id.country_spinner);
        final String[] countryValues;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.country_short,
                R.layout.spinner_item_white_text);

        adapter.setDropDownViewResource(R.layout.spinner_item);
        countrySpinner.setAdapter(adapter);

        countryValues = getResources().getStringArray(R.array.country_value);
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    continueBtn.setVisibility(View.GONE);
                    return;
                } else {
                    continueBtn.setVisibility(View.VISIBLE);
                }

                countryValue = countryValues[position];
                SharedPref.setString(SplashActivity.this, R.string.SELECTED_COUNTRY, countryValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                countrySpinner.setSelection(0);
            }
        });
    }

    private void animateLogoShowSelector() {
        long animLength = 800;
        long delay = 1500;

        logoWrapper.animate()
                .translationYBy(-250)
                .setDuration(animLength)
                .setStartDelay(delay)
                .start();

        countrySelectorWrapper.setVisibility(View.VISIBLE);
        countrySelectorWrapper.animate()
                .alpha(1f)
                .setDuration(animLength)
                .setStartDelay(delay)
                .start();
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            rootLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            rootLayout.setVisibility(View.VISIBLE);
        }
    };

    private final Handler mHideHandler = new Handler();
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}

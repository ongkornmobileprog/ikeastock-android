package xeno.mobileapp.ikeastocknotifier;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ong on 03/05/2016.
 */
public class ProductDBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "TrackingProductDBHelper.sqlite";
    public static final int DB_VER = 1;
    private Context context;

    public ProductDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase database) {
        String sql;
        sql = "CREATE TABLE products (id INTEGER PRIMARY KEY AUTOINCREMENT, productNum TEXT, targetStock INTEGER);";
        database.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldver, int newver) {
        database.execSQL("DROP TABLE IF EXISTS products");
        onCreate(database);
    }
}
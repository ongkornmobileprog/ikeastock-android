package xeno.mobileapp.ikeastocknotifier;

import android.content.Context;

/**
 * Created by Ong on 03/05/2016.
 */
public class SharedPref {
    public static boolean getBool(Context context, int key, boolean defaultValue){
        boolean obj = context.getSharedPreferences(context.getString(key), Context.MODE_PRIVATE)
                .getBoolean(context.getString(key), defaultValue);
        return obj;
    }

    public static void setBool(Context context, int key, boolean value){
        context.getSharedPreferences(context.getString(key), Context.MODE_PRIVATE).edit()
                .putBoolean(context.getString(key), value)
                .apply();
    }

    public static String getString(Context context, int key, String defaultValue){
        String obj = context.getSharedPreferences(context.getString(key), Context.MODE_PRIVATE)
                .getString(context.getString(key), defaultValue);
        return obj;
    }

    public static void setString(Context context, int key, String value){
        context.getSharedPreferences(context.getString(key), Context.MODE_PRIVATE).edit()
                .putString(context.getString(key), value)
                .apply();
    }

    public static String getSelectedCountry(Context context){
        return SharedPref.getString(context, R.string.SELECTED_COUNTRY, "th/en");
    }


}

package xeno.mobileapp.ikeastocknotifier;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Created by Ong on 02/05/2016.
 */
public class Product {

    public static final String SELF_SERVE_SHELF = "BOX_SHELF";
    public static final String SHOWROOM = "CONTACT_STAFF";

    public String productNumber;
    public int targetStock;
    public ArrayList<Stock> stocks;

    public Product(String productNumber){
        this.productNumber = productNumber;
        stocks = new ArrayList<>();
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String storeInStock(){
        for(int i=0; i<stocks.size(); i++){
            Log.d("IKEA Stock", "Store: "+stocks.get(i).storeNumber+" Stock: "+stocks.get(i).stockCount);
            if (stocks.get(i).stockCount > 1) return stocks.get(i).storeNumber;
            break;
        }

        return "";
    }

    @Override
    public String toString() {
        String stockString = "";

        for(int i=0; i<stocks.size(); i++){
            stockString += stocks.get(i).toString();
            if (i != stocks.size()-1) stockString += "\n\n";
            else if (i == stocks.size()-1) stockString += "\n\n\n";
        }

        return "--- Product number: " + productNumber + " ---\n\n" + stockString;
    }

    public String[] getMaxStock(){
        int maxStock = 0;
        String maxStockStore = "";
        for(int i=0; i<stocks.size(); i++){
            if (stocks.get(i).stockCount > maxStock){
                maxStock = stocks.get(i).stockCount;
                maxStockStore = stocks.get(i).storeNumber;
            }
        }

        String[] output = new String[2];
        output[0] = maxStock+"";
        output[1] = maxStockStore;

        return output;
    }

    public void setStocksWithXml(String xml) {
        ArrayList<Stock> tempStocks = new ArrayList<>();
        Stock stock = new Stock(this);
        String appName = "IKEA Stock Notifier";

        try {
            InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlFactoryObject.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(stream, null);

            int event = parser.getEventType();
            String text = null;

            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();

                switch (event){
                    case XmlPullParser.START_TAG:
                        if (name.equals("localStore")){

                            String buCode = parser.getAttributeValue(0);
                            stock = new Stock(this);
                            tempStocks.add(stock);
                            stock.setStoreNumber(buCode);

//                            Log.d(appName,"localStore: buCode = " + buCode);
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        switch (name) {
                            case "availableStock":

//                                Log.d(appName, "availableStock: " + text);
                                stock.setStockCount(Integer.parseInt(text));

                                break;
                            case "inStockProbabilityCode":

//                                Log.d(appName, "inStockProbabilityCode: " + text);
                                stock.setInStockProp(text);

                                break;
//                            case "partNumber":
//
//                                Log.d(appName, "partNumber: " + text);
//                                if (!this.productNumber.equals(text)){
//                                    Log.e(appName, "this.productNumber "+this.productNumber+" != "+text+" xml.productNumber");
//                                }
//
//                                break;
                        }
                        break;
                }
                event = parser.next();
            }
            stream.close();

        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        this.stocks = tempStocks;
    }

    public static ArrayList<Product> getAllProductInDB(Context context){
        Log.d("DB", "getAllProductInDB");

        ArrayList<Product> list = new ArrayList<>();
        ProductDBHelper productDBHelper = new ProductDBHelper(context);
        SQLiteDatabase db = productDBHelper.getReadableDatabase();
        String sql = "SELECT * FROM products ORDER BY productNum";
        Cursor sqlList = db.rawQuery(sql, null);
        if (! sqlList.moveToFirst()) {
            return list;
        }
        do {
            Product product = new Product( sqlList.getString(1));
            product.targetStock = sqlList.getInt(2);
            list.add(product);
        } while (sqlList.moveToNext());

        sqlList.close();
        db.close();
        productDBHelper.close();

        return list;
    }

    public static int getIdInDB(Context context, String productNum){
        Log.d("DB", "getAllProductInDB");

        ArrayList<Product> list = new ArrayList<>();
        ProductDBHelper productDBHelper = new ProductDBHelper(context);
        SQLiteDatabase db = productDBHelper.getReadableDatabase();
        String sql = "SELECT * FROM products ORDER BY productNum";
        Cursor sqlList = db.rawQuery(sql, null);
        if (! sqlList.moveToFirst()) {
            return -1;
        }
        do {
            if(sqlList.getString(1).equals(productNum)){
                return sqlList.getInt(0);
            }
        } while (sqlList.moveToNext());

        sqlList.close();
        db.close();
        productDBHelper.close();

        return -1;
    }

    public static void addProductToDB(Context context, String productNumber, int targetStock){
        try {
            Log.d("DB", "addProductToDB");

            ProductDBHelper dbHelper = new ProductDBHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String sql = "INSERT INTO products (productNum,targetStock) VALUES(?,?)";
            db.execSQL(sql, new String[] {productNumber,targetStock+""});


            db.close();
            dbHelper.close();

            Toast.makeText(
                    context,
                    "Add product "+productNumber+" to tracking list",
                    Toast.LENGTH_LONG
            ).show();
        } catch (SQLiteConstraintException e){
            Toast.makeText(
                    context,
                    "Already added product "+productNumber+" to tracking list",
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    public static void deleteProductFromDB (Context context, String productNum){
        Log.d("DB", "deleteProductFromDB productNum="+productNum);

        ProductDBHelper dbHelper = new ProductDBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        boolean deleteResult = db.delete("products", "id="+Product.getIdInDB(context, productNum), null) > 0;

        if(!deleteResult){
            Log.d("DB", productNum+" not deleted");
            ArrayList<Product> products = getAllProductInDB(context);
            for(int i=0; i<products.size(); i++){
                Log.d("DB", i+") Id in DB = "+Product.getIdInDB(context, products.get(i).productNumber));
            }
        } else {
            Log.d("DB",productNum+" deleted");
        }

        db.close();
        dbHelper.close();
    }

    public static void clearProductDB(Context context){
        Log.d("DB", "clearProductDB");

        ProductDBHelper dbHelper = new ProductDBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("delete from products");

        db.close();
        dbHelper.close();
    }
}

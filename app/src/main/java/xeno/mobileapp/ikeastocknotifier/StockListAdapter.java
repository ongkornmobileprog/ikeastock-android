package xeno.mobileapp.ikeastocknotifier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ong on 03/05/2016.
 */
public class StockListAdapter extends ArrayAdapter<Stock> {

    private LayoutInflater inflater;

    public StockListAdapter(Context context, int resource, ArrayList<Stock> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View roll = convertView;
        final Holder holder;

        if(roll == null){
            if(inflater == null) inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            roll = inflater.inflate(R.layout.item_simple_text, parent, false);
            holder = new Holder(roll);
            roll.setTag(holder);
        } else {
            holder = (Holder) roll.getTag();
        }

        Stock item = getItem(position);

        holder.textview.setText(item.toString());

        return roll;
    }

    private class Holder {
        public TextView textview;

        public Holder(View view){
            textview = (TextView) view.findViewById(R.id.textview);
        }
    }
}

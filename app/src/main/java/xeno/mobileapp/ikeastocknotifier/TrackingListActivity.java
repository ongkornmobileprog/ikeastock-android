package xeno.mobileapp.ikeastocknotifier;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class TrackingListActivity extends AppCompatActivity {

    Button startTrackBtn, stopTrackBtn;
    ListView trackingProductListview;
    ArrayList<Product> trackingProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_list);

        if(!SharedPref.getBool(this, R.string.HAS_CREATE_DB, false)){
            createTableInDB();
            SharedPref.setBool(this, R.string.HAS_CREATE_DB, true);
        }

        trackingProducts = Product.getAllProductInDB(this);

        UpdateTrackingStockTask task = new UpdateTrackingStockTask();
        task.execute((Void) null);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        setupStartTrackBtn();
        setupStopTrackBtn();
        updateStartStopTrackingBtn();
    }

    private void createTableInDB(){
        ProductDBHelper dbHelper = new ProductDBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.close();
        dbHelper.close();
    }

    private void setupTrackingProductListview() {
        trackingProductListview = (ListView) findViewById(R.id.tracking_product_listview);

        TrackingListAdapter adapter = new TrackingListAdapter(this, R.layout.item_track, trackingProducts);
        trackingProductListview.setAdapter(adapter);
    }

    private void updateStartStopTrackingBtn() {
        boolean isTaskRunning = SharedPref.getBool(this, R.string.IS_TRACKING, false);

        if (isTaskRunning){
            startTrackBtn.setVisibility(View.GONE);
            stopTrackBtn.setVisibility(View.VISIBLE);
        } else {
            startTrackBtn.setVisibility(View.VISIBLE);
            stopTrackBtn.setVisibility(View.GONE);
        }
    }

    private void setupStopTrackBtn() {
        stopTrackBtn = (Button) findViewById(R.id.stop_track);
        stopTrackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StockTrackingIntentService.startActionStopTracking(TrackingListActivity.this);

                startTrackBtn.setVisibility(View.VISIBLE);
                stopTrackBtn.setVisibility(View.GONE);
            }
        });
    }

    private void setupStartTrackBtn() {
        startTrackBtn = (Button) findViewById(R.id.start_track);
        startTrackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StockTrackingIntentService.startActionStartTracking(TrackingListActivity.this);

                startTrackBtn.setVisibility(View.GONE);
                stopTrackBtn.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    public class UpdateTrackingStockTask extends AsyncTask<Void, Void, Void> {

        private HTTPHelper httpHelper = new HTTPHelper();

        @Override
        protected void onPreExecute() {
            Toast.makeText(TrackingListActivity.this, "Checking stock ...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            for (int i = 0; i< trackingProducts.size(); i++) {
                Product iProduct = trackingProducts.get(i);

                String respond = httpHelper.GET(
                        getString(R.string.IKEA_PREFIX_XML_1) +
                                SharedPref.getSelectedCountry(TrackingListActivity.this) +
                                getString(R.string.IKEA_PREFIX_XML_2) +
                                iProduct.productNumber
                );

                if (respond == null) {
                    Log.d (getString(R.string.app_name), "null respond");
                    continue;
                } else {
                    Log.d (getString(R.string.app_name), "respond XML of " + iProduct.productNumber);
                }

                iProduct.setStocksWithXml(respond);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            setupTrackingProductListview();
        }
    }
}

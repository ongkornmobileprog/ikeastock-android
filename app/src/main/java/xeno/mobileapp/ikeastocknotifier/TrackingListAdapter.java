package xeno.mobileapp.ikeastocknotifier;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ong on 03/05/2016.
 */
public class TrackingListAdapter extends ArrayAdapter<Product> {

    private LayoutInflater inflater;
    private int resource;

    public TrackingListAdapter(Context context, int resource, ArrayList<Product> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View roll = convertView;
        final Holder holder;

        if(roll == null){
            if(inflater == null) inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            roll = inflater.inflate(resource, parent, false);
            holder = new Holder(roll);
            roll.setTag(holder);
        } else {
            holder = (Holder) roll.getTag();
        }

        final Product item = getItem(position);

        String output = "";
        output += "Product Number: " + item.productNumber + "\n";
        output += "Stock: " + item.getMaxStock()[0]+ "/" +item.targetStock + "\n";
        output += "Store Number: " + (item.getMaxStock()[1].equals("") ? "N/A":item.getMaxStock()[1]);
        holder.textview.setText(output);

        holder.ikeaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(
                        getContext().getString(R.string.IKEA_PREFIX_VIEW_PRODUCT_1) +
                        SharedPref.getSelectedCountry(getContext()) +
                        getContext().getString(R.string.IKEA_PREFIX_VIEW_PRODUCT_2) +
                        item.productNumber + "/"
                );
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                getContext().startActivity(intent);
            }
        });

        return roll;
    }

    private class Holder {
        public TextView textview;
        public ImageButton ikeaBtn;

        public Holder(View view){
            textview = (TextView) view.findViewById(R.id.textview);
            ikeaBtn = (ImageButton) view.findViewById(R.id.view_product_btn);
        }
    }
}

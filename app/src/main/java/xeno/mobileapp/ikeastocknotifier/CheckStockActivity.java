package xeno.mobileapp.ikeastocknotifier;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class CheckStockActivity extends AppCompatActivity implements DownloadResultReceiver.Receiver {

    public static String appName;

    ImageButton checkStockBtn, viewProductBtn;
    Button addTrackingProductBtn, showAllTrackBtn;
    EditText productNumEdt;
    ListView stockListview;
    Product product;
    String countryValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_stock);

        appName = getString(R.string.app_name);
        countryValue = SharedPref.getSelectedCountry(this);

        Log.d(appName, "onCreate");

        setupSearchBtn();
        setupViewProductBtn();
        setupAddTrackingProductBtn();
        setupShowAllTrackBtn();

        handleShareIntentFromBrowser();
    }

    private void handleShareIntentFromBrowser() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                Log.d(appName, "Share from web intent\ntext/plain = "+sharedText);

                if(!(sharedText.toLowerCase().contains("m.ikea.com") ||
                        sharedText.toLowerCase().contains("www.ikea.com")))
                {
                    Toast.makeText(this, "Please share link from IKEA website", Toast.LENGTH_LONG).show();
                    return;
                }

                String country = SharedPref.getSelectedCountry(this);
                if(!sharedText.toLowerCase().contains(country.substring(0, 2).toLowerCase())){
                    Toast.makeText(this, "Please share link from local IKEA website", Toast.LENGTH_LONG).show();
                    return;
                }

                int slashCount = 0;
                for(int i=sharedText.length()-1; i>=0; i--){
                    if(sharedText.charAt(i) == '/'){
                        slashCount++;
                        if(slashCount == 2){
                            String productNum = sharedText.substring(i+1, sharedText.length()-1);
                            Log.d(appName, "productNum = "+productNum);

                            checkStock(productNum);
                            break;
                        }
                        if(slashCount > 2) return;
                    }
                }
            }
        }
    }

    private void checkStock(String productNum){
        productNumEdt.setText(productNum+"");
        checkStockBtn.callOnClick();
    }

    private void setupShowAllTrackBtn() {
        showAllTrackBtn = (Button) findViewById(R.id.view_all_tracking_product);
        showAllTrackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent trackingListIntent = new Intent(CheckStockActivity.this, TrackingListActivity.class);
                startActivity(trackingListIntent);
            }
        });
    }

    private void setupAddTrackingProductBtn() {
        addTrackingProductBtn = (Button) findViewById(R.id.add_to_tracking_list_btn);
        addTrackingProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StockTrackingIntentService.startActionAddTrackingProduct(
                        CheckStockActivity.this,
                        productNumEdt.getText().toString(),
                        1
                );
                showAllTrackBtn.callOnClick();
            }
        });

        addTrackingProductBtn.setVisibility(View.GONE);
    }

    private void setupViewProductBtn() {
        viewProductBtn = (ImageButton) findViewById(R.id.view_product_btn);
        viewProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri;

                if(productNumEdt.getText().toString().length()==0){
                    uri = Uri.parse("http://m.ikea.com/" + countryValue);
                } else {
                    uri = Uri.parse(
                            getString(R.string.IKEA_PREFIX_VIEW_PRODUCT_1) +
                                    countryValue +
                                    getString(R.string.IKEA_PREFIX_VIEW_PRODUCT_2) +
                                    productNumEdt.getText().toString() + "/"
                    );
                }
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    private void setupSearchBtn() {
        checkStockBtn = (ImageButton) findViewById(R.id.check_stock_btn);
        productNumEdt = (EditText) findViewById(R.id.product_num_edittext);
        stockListview = (ListView) findViewById(R.id.stock_listview);
        stockListview.setVisibility(View.GONE);

        checkStockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product = new Product(productNumEdt.getText().toString());

                CheckStockTask checkStockTask = new CheckStockTask(productNumEdt.getText().toString());
                checkStockTask.execute((Void) null);
            }
        });
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //TODO
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.check_stock_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.change_country:
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("Change Country")
                    .setMessage("This will remove any products being tracked.")
                    .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Product.clearProductDB(CheckStockActivity.this);
                            SharedPref.setBool(CheckStockActivity.this, R.string.HAS_CREATE_DB, false);
                            SharedPref.setString(CheckStockActivity.this, R.string.SELECTED_COUNTRY, "changeCountry");
                            Intent splashIntent = new Intent(CheckStockActivity.this, SplashActivity.class);
                            splashIntent.putExtra("changeCountry", true);
                            startActivity(splashIntent);
                            finish();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();

            return true;
        default:
            return super.onOptionsItemSelected(item);
    }
    }

    public class CheckStockTask extends AsyncTask<Void, Void, String> {

        private String mProductNumber;

        CheckStockTask (String productNumber) {
            mProductNumber = productNumber;
        }

        @Override
        protected String doInBackground(Void... params) {
            HTTPHelper httpHelper = new HTTPHelper();
            String respond = httpHelper.GET(
                    getString(R.string.IKEA_PREFIX_XML_1) +
                    countryValue +
                    getString(R.string.IKEA_PREFIX_XML_2) +
                    mProductNumber
            );

            if (respond == null) {
                Log.d (appName, "null respond");
                return "";
            } else {
                Log.d (appName, "respond: " + respond);
            }

            return respond;
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(CheckStockActivity.this, "Checking stock ...", Toast.LENGTH_SHORT).show();
        }



        @Override
        protected void onPostExecute(String respond) {
            if (respond.equals("")) {
                Toast.makeText(CheckStockActivity.this, "Invalid product code", Toast.LENGTH_SHORT).show();
                product = null;
                stockListview.setAdapter(new StockListAdapter(
                        CheckStockActivity.this,
                        R.layout.item_simple_text,
                        new ArrayList<Stock>()
                ));
                stockListview.setVisibility(View.GONE);
                addTrackingProductBtn.setVisibility(View.GONE);
            } else {
                product.setStocksWithXml(respond);

                StockListAdapter adapter = new StockListAdapter(
                        CheckStockActivity.this,
                        R.layout.item_simple_text,
                        product.stocks
                );
                if(isOutOfStock(product)){
                    addTrackingProductBtn.setVisibility(View.VISIBLE);
                } else {
                    addTrackingProductBtn.setVisibility(View.GONE);
                }

                stockListview.setAdapter(adapter);
                stockListview.setVisibility(View.VISIBLE);
            }
        }

        private boolean isOutOfStock(Product product){
            for(int i=0; i<product.stocks.size(); i++){
                if(product.stocks.get(i).stockCount > 0) return false;
            }
            return true;
        }
    }
}

package xeno.mobileapp.ikeastocknotifier;

/**
 * Created by Ong on 03/05/2016.
 */
public class Stock {
    public String storeNumber, inStockProp;
    public int stockCount;

    public Product mProduct;

    public Stock (Product product) {
        mProduct = product;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public void setInStockProp(String inStockProp) {
        this.inStockProp = inStockProp;
    }

    public void setStockCount(int stockCount) {
        this.stockCount = stockCount;
    }

    @Override
    public String toString() {
        return "-- Store number: " + storeNumber + " --\n" +
                "Stock: " + stockCount + "\n" +
                "Stock Probability: " + inStockProp;
    }
}

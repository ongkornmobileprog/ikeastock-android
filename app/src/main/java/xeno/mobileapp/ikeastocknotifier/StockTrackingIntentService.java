package xeno.mobileapp.ikeastocknotifier;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.os.Handler;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class StockTrackingIntentService extends IntentService {

    private static final String ACTION_START_TRACKING = "xeno.mobileapp.ikeastocknotifier.action.START_TRACKING";
    private static final String ACTION_STOP_TRACKING = "xeno.mobileapp.ikeastocknotifier.action.STOP_TRACKING";
    private static final String ACTION_ADD_TRACKING_PRODUCT = "xeno.mobileapp.ikeastocknotifier.action.ADD_TRACKING_PRODUCT";

    public static final String PARAM_PRODUCT_NUM = "productNum";
    public static final String PARAM_TARGET_STOCK = "targetStock";

    public static final int UPDATE_INTERVAL = 600000;

    public ArrayList<Product> trackingProducts;
    public Handler trackLoopHandler;
    public Runnable trackLoopRunnable;
    public TrackStockTask trackStockTask;
    public static boolean isTaskRunning = false;

    public StockTrackingIntentService() {
        super("StockTrackingIntentService");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        trackLoopHandler = new Handler();
        trackStockTask = new TrackStockTask();
        trackLoopRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    isTaskRunning = true;
                    SharedPref.setBool(StockTrackingIntentService.this, R.string.IS_TRACKING, true);
                    sendKeepTrackNotification();
                    new TrackStockTask().execute((Void) null);
                } finally {
                    trackLoopHandler.postDelayed(trackLoopRunnable, UPDATE_INTERVAL);
                }
            }
        };

        trackingProducts = Product.getAllProductInDB(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(getString(R.string.app_name), "onStartCommand");

        onStart(intent, startId);

        isTaskRunning = SharedPref.getBool(this, R.string.IS_TRACKING, false);

        if (isTaskRunning) {
            trackLoopRunnable.run();
        }

        if (intent != null) {
            onHandleIntent(intent);
        }

        return START_STICKY;
    }

    public static void startActionAddTrackingProduct(Context context, String productNumber, int targetStock) {
        Intent intent = new Intent(context, StockTrackingIntentService.class);
        intent.setAction(ACTION_ADD_TRACKING_PRODUCT);
        intent.putExtra(PARAM_PRODUCT_NUM, productNumber);
        intent.putExtra(PARAM_TARGET_STOCK, targetStock);
        context.startService(intent);
    }

    public static void startActionStartTracking(Context context) {
        Intent intent = new Intent(context, StockTrackingIntentService.class);
        intent.setAction(ACTION_START_TRACKING);
        context.startService(intent);
    }

    public static void startActionStopTracking(Context context) {
        Intent intent = new Intent(context, StockTrackingIntentService.class);
        intent.setAction(ACTION_STOP_TRACKING);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            trackingProducts = Product.getAllProductInDB(this);
            Log.d("DB", "trackingProducts.size() = "+trackingProducts.size());

            final String action = intent.getAction();
            if (ACTION_START_TRACKING.equals(action)) {

                startTracking();

            } else if (ACTION_STOP_TRACKING.equals(action)){

                stopTracking();

            } else if (ACTION_ADD_TRACKING_PRODUCT.equals(action)){

                final String productNumber = intent.getStringExtra(PARAM_PRODUCT_NUM);
                final int targetStock = intent.getIntExtra(PARAM_TARGET_STOCK, -1);
                if (productNumber.equals("") || targetStock == -1) return;

                addTrackingProduct(productNumber, targetStock);
            }
        }
    }

    private void startTracking() {

        Log.d(getString(R.string.app_name), "Start tracking " + trackingProducts.size() + " products");

        if (!isTaskRunning){
            trackLoopRunnable.run();
        }
    }

    private void stopTracking() {
        Log.d(getString(R.string.app_name), "Stop tracking");

        if (isTaskRunning) {
            trackLoopHandler.removeCallbacks(trackLoopRunnable);
            isTaskRunning = false;
            getSharedPreferences(getString(R.string.IS_TRACKING), Context.MODE_PRIVATE).edit()
                    .putBoolean(getString(R.string.IS_TRACKING), false)
                    .apply();
        }

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(99129323);
    }

    private void addTrackingProduct(String productNumber, int targetStock) {
        Log.d(getString(R.string.app_name),
                "addTrackingProduct(productNumber="+productNumber+ ", targetStock="+targetStock+")"
        );

        Product.addProductToDB(this, productNumber, targetStock);

        Product product = new Product(productNumber);
        product.productNumber = productNumber;
        product.targetStock = targetStock;
        trackingProducts.add(product);

        if (!isTaskRunning) {
            startTracking();
        }
    }

    public void sendKeepTrackNotification(){
        Intent notificationIntent = new Intent(this, CheckStockActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(StockTrackingIntentService.this, 0, notificationIntent, 0);

        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());

        Notification notification = new NotificationCompat.Builder(StockTrackingIntentService.this)
                .setContentTitle("Tracking "+trackingProducts.size()+" products")
                .setContentText("Last update: " + date)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_shopping_basket_accent)
                .setAutoCancel(true)
                .build();

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(99129323, notification);
    }

    public class TrackStockTask extends AsyncTask <Void, Void, Void> {

        private HTTPHelper httpHelper = new HTTPHelper();

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i< trackingProducts.size(); i++) {
                Product iProduct = trackingProducts.get(i);

                String respond = httpHelper.GET(
                        getString(R.string.IKEA_PREFIX_XML_1) +
                                SharedPref.getSelectedCountry(StockTrackingIntentService.this) +
                                getString(R.string.IKEA_PREFIX_XML_2) +
                                iProduct.productNumber
                );

                Log.d("IKEA Stock", "Stored Location: "+SharedPref.getSelectedCountry(StockTrackingIntentService.this));

                if (respond == null) {
                    Log.d (getString(R.string.app_name), "null respond");
                    continue;
                } else {
                    Log.d (getString(R.string.app_name), "respond XML of " + iProduct.productNumber);
                }

                iProduct.setStocksWithXml(respond);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            for(int i=0; i<trackingProducts.size(); i++){
                Product iProduct = trackingProducts.get(i);
                if (!iProduct.storeInStock().equals("")){
                    Log.d(getString(R.string.app_name), "product "+iProduct.productNumber+" is in-stock at Store "+iProduct.storeInStock());

                    sendInStockNotification(iProduct);

                    Product.deleteProductFromDB(getApplicationContext(), iProduct.productNumber);
                }
            }
        }

        public void sendInStockNotification(Product product){
            Intent notificationIntent = new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                            getString(R.string.IKEA_PREFIX_VIEW_PRODUCT_1) +
                            SharedPref.getSelectedCountry(StockTrackingIntentService.this) +
                            getString(R.string.IKEA_PREFIX_VIEW_PRODUCT_2) +
                            product.productNumber + "/"
                    )
            );

            PendingIntent contentIntent = PendingIntent.getActivity(StockTrackingIntentService.this, 0, notificationIntent, 0);

            Notification notification = new NotificationCompat.Builder(StockTrackingIntentService.this)
                    .setContentTitle("IKEA Product In-stock. Go get it!")
                    .setContentText("Product "+product.productNumber+" is in-stock at Store "+product.storeInStock())
                    .setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.ic_shopping_basket_accent)
                    .setAutoCancel(true)
                    .build();

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            java.util.Random random = new java.util.Random();
            mNotificationManager.notify(random.nextInt(), notification);
        }
    }
}
